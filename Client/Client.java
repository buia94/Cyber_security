
import java.util.Scanner;
import javax.net.ssl.SSLSocketFactory;


import java.io.BufferedReader;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.net.Socket;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocket;

public class Client {

  private PasswordManager passwordManager;
  private final int keyLength = 256;
  private final int keyIterations = 65536;
  private final String algorithmToEncrypt = "AES/CBC/PKCS5Padding"; //uso aes 256 bit con cbc per cifreare le password dei siti da salvare
  private final List<String> availableCommands = new ArrayList<>(Arrays.asList("get_sites", "add_site", "log_out"));
  private final int serverPort;
  private final String serverAddress;
  private Socket socket;
  private PrintWriter  stringOutputStream;
  private BufferedReader stringInputStream;
  private ObjectOutputStream objectOutputStream;
  private ObjectInputStream objectInputStream;
  private final Scanner scanner;
  private String username;
  private String password;
  private String salt;

  public Client(int port, String address, String trustStore) {
    this.serverPort=port;
    this.serverAddress=address;
    System.setProperty("javax.net.ssl.trustStore", trustStore);
    this.scanner = new Scanner(System.in);
    this.passwordManager = new PasswordManager(this.keyLength,this.keyIterations,this.algorithmToEncrypt);
  }


  /*Stampa informazioni sul protocollo ssl (cipher suite usata ecc)*/
  static void printInfoCommunication(SSLSession session){
    try {
      System.out.println("\n-------------------Info SSL session-------------------");
      Certificate[] cchain = session.getPeerCertificates();
      System.out.println("The Certificates used by peer");
      for (Certificate cchain1 : cchain) {
        System.out.println(((X509Certificate) cchain1).getSubjectDN());
      }
      System.out.println("Peer host is " + session.getPeerHost());
      System.out.println("Cipher is " + session.getCipherSuite());
      System.out.println("Protocol is " + session.getProtocol());
      System.out.println("ID is " + new BigInteger(session.getId()));
      System.out.println("Session created in " + session.getCreationTime());
      System.out.println("Session accessed in " + session.getLastAccessedTime());
      System.out.println("----------------------------------------------------\n");
    } catch (SSLPeerUnverifiedException ex) {
      Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
    }
  }

  /*Stampa i comandi disponibili, che l'utente può inviare al server*/
  private void printAvailableCommands(){
    System.out.println("\n-------------------The available commands are--------------------");
    this.availableCommands.forEach(s->System.out.println(s));
    System.out.println("-----------------------------------------------------------------\n");
  }

  /*Invia le credenziali d'accesso al server (username e password)*/
  private boolean sendCredentials() throws IOException{
    try {
      System.out.println("Insert username");
      this.username = this.scanner.nextLine();
      stringOutputStream.println(this.username);  //Invio username, così che il server mi rinvii il salt da usare
      ServerResponseMessage usernamePresent = (ServerResponseMessage) this.objectInputStream.readObject();
      System.out.println("The server say --> "+usernamePresent.text+"\n");
      if(usernamePresent.getRequestResult()){
        this.salt = stringInputStream.readLine(); //Il server mi spedisce il salt da usare nella creazione dell'hash e della chiave. Sarà assegnato in fase di registrazione e poi sarà mantenuto sempre lo stesso
        System.out.println("The user salt is --> " + this.salt+"\n");
        passwordManager.setSalt(this.salt);

        System.out.println("Insert password");
        this.password = this.scanner.nextLine();
        passwordManager.setMasterPassword(this.password);
        String passwordHash = passwordManager.getPasswordHash();
        //System.out.println("The hash of the password to send to the server is --> " + passwordHash);

        stringOutputStream.println(passwordHash); //invio password hash al server

        ServerResponseMessage authenticationState = (ServerResponseMessage) this.objectInputStream.readObject();
        System.out.println("The server say --> "+authenticationState.text+"\n");

        return authenticationState.getRequestResult();
      }
    }
    catch (ClassNotFoundException ex) {
      Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
    }
    return false;
  }

  /*Stampa le informazioni sui siti salvati nel server dopo che quest'ultimo li restituise. La password verrà mostrata a video decifrata.*/
  private void printSites(UserSites userSites){
    System.out.println("\n--------------The user sites are--------------------------------");
    userSites.sitesList.forEach(((site)->{
    String plainPassword = passwordManager.decryptText(site.chiperPassword,site.chiperIv);
    System.out.println("Site name --> "+site.siteName+"\nUsername --> "+site.username+"\nPassword --> "+plainPassword+"\nCipherIV --> "+site.chiperIv);}));
    System.out.println("-----------------------------------------------------------------\n");
  }

  /*Richiede i siti al server*/
  private boolean getSites()  throws IOException{
    try {
      stringOutputStream.println(availableCommands.get(0));
      ServerResponseMessage requestState = (ServerResponseMessage) this.objectInputStream.readObject();
      System.out.println("The server say --> "+requestState.text);
      if(requestState.getRequestResult()){
        UserSites userSites = (UserSites) this.objectInputStream.readObject();
        printSites(userSites);
      }
      return true;
    }
    catch (EOFException ex) {
      System.out.println("Fine stream --> server disconnected");
    }
    catch (ClassNotFoundException ex) {
      Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
    }
    return false;
  }

  /*Aggiunge le varie informazioni di un sito sul server. La passowrd verrà inviata al server cifrata con AES/CBC, tramite la chiave estratta dalla password*/
  private boolean addSite() throws IOException{
    try {
      System.out.println("Insert site name");
      String siteName = this.scanner.nextLine();
      System.out.println("Insert site username");
      String siteUsername = this.scanner.nextLine();
      System.out.println("Insert site password");
      String sitePassword = this.scanner.nextLine();
      ArrayList<String> encryptReturn = passwordManager.encryptText(sitePassword);
      String cipherPassword = encryptReturn.get(0);
      String cipherIv = encryptReturn.get(1);
      //System.out.println("The site cipher password is --> " + cipherPassword);
      Site site = new Site(siteName, siteUsername, cipherPassword, cipherIv);
      stringOutputStream.println(availableCommands.get(1));
      objectOutputStream.writeObject(site);

      ServerResponseMessage requestState = (ServerResponseMessage) this.objectInputStream.readObject();
      System.out.println("The server say --> "+requestState.text+"\n");
      return true;
    } catch (ClassNotFoundException ex) {
      Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
    }
    return false;
  }

  /*Invio un comando al server, tra quell disponbili*/
  private boolean sendCommand() throws IOException{
    printAvailableCommands();
    System.out.println("Insert command");
    String command = this.scanner.nextLine();
    switch (command) {
      case "get_sites": /*comando get_sites*/
      return getSites();
      case "add_site":  /*comando add_site*/
      return addSite();
      case "log_out":  /*comando log_out*/
      System.out.println("Successful logout");
      return false;
      default: /*comando non disponibile*/
      System.out.println("The server say --> Command not available\n");
      return true;
    }
  }

  /*crea il socket ssl, i vari stream e fa partire la connessione*/
  public void startConnection() {
    SSLSocketFactory sslSocketFactory = (SSLSocketFactory)SSLSocketFactory.getDefault();
    try {
      /*Open socket and create ssl connection*/
      this.socket = sslSocketFactory.createSocket(this.serverAddress, this.serverPort);

      SSLSession session = ((SSLSocket) socket).getSession();
      printInfoCommunication(session);

      /*Apro i vari stream per leggere e inviare tramite il socket, stringhe o oggetti*/
      this.stringOutputStream = new PrintWriter(this.socket.getOutputStream(), true);
      this.stringInputStream = new BufferedReader( new InputStreamReader( this.socket.getInputStream()));
      this.objectOutputStream=new ObjectOutputStream(this.socket.getOutputStream());
      this.objectInputStream=new ObjectInputStream(this.socket.getInputStream());

      while(!sendCredentials()){
      }

      while(sendCommand()){
      }

    }
    catch (IOException ex) {
      Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
    }
    finally{
      closeConnection();
    }
  }

  /*Chiude i vari stream e il socket*/
  private void closeConnection(){
    try {
      this.stringOutputStream.close();
      this.stringInputStream.close();
      this.objectOutputStream.close();
      this.objectInputStream.close();
      this.socket.close();
      this.scanner.close();
    }
    catch (IOException ex) {
      Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
    }
  }
  }
