
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.net.ssl.SSLServerSocketFactory;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


public class Server {

  private final int serverPort;
  private final ExecutorService executor;
  private final String usersFolder = "./Utenti/";
  private final String fileUsers = "credentials.bin";
  private final FileManager fileManager;

  public Server(int port,int numThread, String keyStorePassword, String keyStore) {
    this.serverPort = port;
    this.executor = Executors.newFixedThreadPool(numThread);
    System.setProperty("javax.net.ssl.keyStore", keyStore);
    System.setProperty("javax.net.ssl.keyStorePassword", keyStorePassword);
    this.fileManager=new FileManager(usersFolder,fileUsers);
  }

  public void startConnection() {
    SSLServerSocketFactory sslServerSocketFactory = (SSLServerSocketFactory)SSLServerSocketFactory.getDefault();

    try {
      ServerSocket sslServerSocket = sslServerSocketFactory.createServerSocket(this.serverPort);
      System.out.println("SSL ServerSocket started\n" + sslServerSocket.toString()); //stampo info server socket

      /*ascolta nel socket di ascolto e restituisci un socket di comunicazione (da li in poi va assegnato ad un thread)*/
      while(true){
        Socket socket = sslServerSocket.accept();
        System.out.println("ServerSocket accepted");
        ServerTask serverTask = new ServerTask(socket,fileManager);
        this.executor.submit(serverTask);
        }

      //this.executor.shutdown();
    }

    catch (IOException ex) {
      Logger.getLogger(Server.class.getName()) .log(Level.SEVERE, null, ex);
      }
    }
  }
