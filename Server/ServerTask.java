import java.io.BufferedReader;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.net.Socket;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocket;


public class ServerTask implements Runnable{
    private final int keyLength = 256;
    private final int keyIterations = 65536;
    private final List<String> availableCommands = new ArrayList<>(Arrays.asList("get_sites", "add_site", "log_out"));
    private final Socket socket;
    private PrintWriter stringOutputStream;
    private BufferedReader stringInputStream;
    private ObjectOutputStream objectOutputStream;
    private ObjectInputStream objectInputStream;
    private String username;
    private FileManager fileManager;
    private boolean userDisconnected; //nel caso l'utente si disconnetta durante l'invio delle credenziali
    private PasswordManager passwordManager;

    public ServerTask(Socket s,FileManager fileManager){
        this.socket = s;
        this.userDisconnected = false;
        this.fileManager = fileManager;
        this.passwordManager = new PasswordManager(this.keyLength, this.keyIterations);
    }

    /*Stampa informazioni sul protocollo ssl (cipher suite usata ecc)*/
    static void printInfoCommunication(SSLSession session){
      System.out.println("\n-------------------Info SSL session-------------------");
      Certificate[] cchain2 = session.getLocalCertificates();
        for (Certificate cchain21 : cchain2) {
            System.out.println(((X509Certificate) cchain21).getSubjectDN());
        }
      System.out.println("Peer host is " + session.getPeerHost());
      System.out.println("Cipher is " + session.getCipherSuite());
      System.out.println("Protocol is " + session.getProtocol());
      System.out.println("ID is " + new BigInteger(session.getId()));
      System.out.println("Session created in " + session.getCreationTime());
      System.out.println("Session accessed in " + session.getLastAccessedTime());
      System.out.println("Thread id --> " + Thread.currentThread().getId());
      System.out.println("----------------------------------------------------\n");
    }


    /*Restituisce l'hash dell'hash della password*/
    private String getDoubleHashPassword(String salt, String passwordHash){
      passwordManager.setSalt(salt);
			passwordManager.setMasterPassword(passwordHash);
      String doubleHashPassword = passwordManager.getPasswordHash();
      return doubleHashPassword;
    }


    /*Controlla se le credenziali inserite dall'utente sono corrette dato username, l'hash della password e salt*/
    private boolean checkUserAuthentication(String username, String hashPassword, String salt){
      String doubleHashPassword = this.getDoubleHashPassword(salt,hashPassword);
      boolean authenticationSuccess = this.fileManager.checkUser(username, doubleHashPassword); //controllo credenziali nel file
      return authenticationSuccess;
    }


    /*Riceve le credenziali dell'utente e controlla se è presente*/
    private boolean receiveCredentials() throws IOException{
      this.username = this.stringInputStream.readLine(); /*controlla se è NULL esci (fine socket stream)*/
      if(this.username==null){
        this.userDisconnected = true;
        return false;
      }
      String salt = this.fileManager.getSalt(username);
      if(salt==null){        //utente non presente
        System.out.println("Authentication failed: The user " + this.username +" is not present\n");
        objectOutputStream.writeObject(new ServerResponseMessage("ERR", "The username is not present in the server"));
        return false;
      }
      objectOutputStream.writeObject(new ServerResponseMessage("OK", "The username is present in the server"));
      stringOutputStream.println(salt); //invio salt all'utente
      String hashPassword = this.stringInputStream.readLine();
      if(hashPassword==null){
        this.userDisconnected = true;
        return false;
      }
      System.out.println("Received credentials --> " + this.username +"  "+ hashPassword +"\n");
      boolean maxAttemptsExpired = fileManager.checkTimestampUser(this.username);
      if(!maxAttemptsExpired){
        System.out.println("Authentication failed: The " + this.username +" has reached the maximum number of attempts to enter the password");
        objectOutputStream.writeObject(new ServerResponseMessage("ERR", "The user has reached the maximum number of attempts to enter the password"));
        return false;
      }
      boolean authenticationSuccess = this.checkUserAuthentication(this.username, hashPassword, salt);
      if(authenticationSuccess){
        objectOutputStream.writeObject(new ServerResponseMessage("OK", "Authentication successful"));
        fileManager.resetTimestampUser(this.username);
        return true;
      }
      System.out.println("Authentication failed: The password of " + this.username +" is wrong");
      objectOutputStream.writeObject(new ServerResponseMessage("ERR", "Authentication failed: the password is wrong"));
      return false;
    }


    /*prelevo siti dal file con il gestore file e invio una listi di siti se presenti*/
    private boolean sendSites() throws IOException{
      UserSites userSites = this.fileManager.getSites(this.username);
      System.out.println("Read all the sites information of " + this.username + " \n"); /*send error se ci sono problemi*/
      if(userSites==null){
        objectOutputStream.writeObject(new ServerResponseMessage("ERR", "The user has no sites saved on the server"));
      }
      else{
        objectOutputStream.writeObject(new ServerResponseMessage("OK", "The user has sites saved on the server"));
        this.objectOutputStream.writeObject(userSites);
      }
      return true;
    }


    /*Aggiungo sito con il gestore file e invio OK se tutto è andato a buon fine oppure ERR*/
    private boolean addSite() throws IOException{
      try{
        Site site = (Site) this.objectInputStream.readObject();
        System.out.println("Add site information for " +this.username+ "--> " +site.siteName+" "+site.username+" "+site.chiperPassword+"\n");
        boolean insertDone = this.fileManager.insertSite(this.username,site);
        if(insertDone){
          objectOutputStream.writeObject(new ServerResponseMessage("OK", " Site added successfully"));
        }
        else{
          objectOutputStream.writeObject(new ServerResponseMessage("ERR", " Error in site addition"));
        }

        return true;
      }
      catch (EOFException ex) {
        System.out.println("End stream"); //FINE STREAM perchè l'utente si è disconnesso
      }
      catch (ClassNotFoundException ex) {
        Logger.getLogger(ServerTask.class.getName()).log(Level.SEVERE, null, ex);
      }
      return false;
    }


    /*Riceve un comando dal client, controlla se è presente e richiama la funzione che si occupa di gestirlo*/
    public boolean receiveCommand() throws IOException{
      String command = this.stringInputStream.readLine(); /*controlla se è NULL esci*/
      if(command==null){
        return false;
      }
      else{
        System.out.println("Received command from " +this.username+ " --> " + command);
        switch (command) {
          case "get_sites": /*comando get_sites*/
          return sendSites();
          case "add_site":  /*comando add_site*/
          return addSite();
          case "log_out":  /*comando log_out*/
          System.out.println("Log out user");
          return false;
          default: /*comando non disponibile*/
          System.out.println("Command not available"); /*DA VEDERE*/
          return true;
        }
      }
    }


    @Override
    public void run(){
        try {
            this.objectOutputStream = new ObjectOutputStream(this.socket.getOutputStream());
            this.objectInputStream = new ObjectInputStream(this.socket.getInputStream());
            this.stringOutputStream = new PrintWriter(this.socket.getOutputStream(), true);
            this.stringInputStream = new BufferedReader( new InputStreamReader( this.socket.getInputStream()));

            /*stampa informazioni comunicazione*/
            SSLSession session = ((SSLSocket) socket).getSession();
            printInfoCommunication(session);

            /*--------inizia la comunicazione----------*/
            boolean authenticationState = false;
            while(!authenticationState && !this.userDisconnected){
                authenticationState = receiveCredentials();
            }

            boolean commandState = true;
            while(commandState && (!this.userDisconnected)){
              System.out.println("\nUser " + this.username + " ready to send commands\n");
              commandState = receiveCommand();
            }

            /*-------- close everything ----------*/
            if(this.username==null){ //non ha nemmeno fatto il login
              System.out.println("\nThe user has close the ssl connection");
            }
            else{
              System.out.println("\nUser " + this.username + " has logged off");
            }
            this.stringOutputStream.close();
            this.stringInputStream.close();
            this.objectOutputStream.close();
            this.objectInputStream.close();
            this.socket.close();

        } catch (IOException ex) {
            Logger.getLogger(ServerTask.class.getName()).log(Level.SEVERE, null, ex);
        }
        finally{
          closeConnection();
        }
    }

    /*Chiude i vari stream e il socket*/
    private void closeConnection(){
      try {
        this.stringOutputStream.close();
        this.stringInputStream.close();
        this.objectOutputStream.close();
        this.objectInputStream.close();
        this.socket.close();
      }
      catch (IOException ex) {
        Logger.getLogger(ServerTask.class.getName()).log(Level.SEVERE, null, ex);
      }
    }

}
