
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;


public class FileLock
{
  private final ReadWriteLock readWriteLock = new ReentrantReadWriteLock();
  private final Lock readLock = readWriteLock.readLock();
  private final Lock writeLock = readWriteLock.writeLock();
  private final String file;

  public FileLock(String file){
    this.file = file;
  }

  public Lock getWriteLock(){
    return this.writeLock;
  }

  public Lock getReadLock(){
    return this.readLock;
  }

  public String getFileName(){
    return this.file;
  }
}
