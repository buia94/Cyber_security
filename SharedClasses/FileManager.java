
import java.io.*;
import java.util.*;
import java.time.*;
import java.util.concurrent.locks.Lock;


public class FileManager {
	private final String usersFolder;
	private final String fileUsers;
	private final LockManager lockManager = new LockManager();

	public FileManager(String folder,String file){
		usersFolder=folder;
		fileUsers=file;
	}


	/*inizializza la lista degli utenti a dei valori di default prestabiliti*/
	public void setUsersList(List<User> list){

		Credentials c=new Credentials(list);
		Lock writeLock = lockManager.getWriteLock(fileUsers);
		writeLock.lock();
		try(ObjectOutputStream oos=new ObjectOutputStream(new FileOutputStream(fileUsers));){
			oos.writeObject(c);
		}
		catch(Exception e){e.printStackTrace();}
		finally{
			writeLock.unlock();
		}
	}


	/*restituisce true se l'utente è presente nel file delle credenziali*/
	public boolean checkUser(String username, String password){
		Lock readLock = lockManager.getReadLock(fileUsers);
		readLock.lock();
		try(ObjectInputStream ois=new ObjectInputStream(new FileInputStream(fileUsers));){
			Credentials c=(Credentials)ois.readObject();
			for (int i=0;i<c.userList.size();i++){
				if((c.userList.get(i).username.equals(username))&&(c.userList.get(i).hashPassword.equals(password))){
					return true;
				}
			}
			return false;
		}
		catch(Exception e){e.printStackTrace();}
		finally{
			readLock.unlock();
		}
		return false;
	}


	/*restituisce il salt dell'utente*/
	public String getSalt(String username){
		User u=getUser(username);
		if(u!=null)
			return u.salt;
		return null;
	}


	/*restituisce la lista di sites relativi a quell'utente, restituisce null se l'utente non ha salvato nessun sito*/
	public UserSites getSites(String user){
		String path=usersFolder+user+".bin";
		File myFile = new File(path);
		boolean cond=myFile.exists();
		if(!cond)
		return null;
		Lock readLock = lockManager.getReadLock(path);
		readLock.lock();
		try( ObjectInputStream ois=new ObjectInputStream(new FileInputStream(path))){
			UserSites sites=(UserSites)ois.readObject();
			return sites;
		}
		catch(Exception e){e.printStackTrace();}
		finally{
			readLock.unlock();
		}
		return null;
	}


	/*inserisco un sito nel file della lista sites relativi ad un utente*/
	public boolean insertSite(String user,Site site){
		String path=usersFolder+user+".bin";
		List<Site> list;
		UserSites sites=null;
		File myFile = new File(path);
		boolean cond=myFile.exists();//restituisce true se il file esiste
		//sto controllando se il file esiste gia, se esiste leggo il file presente e ci aggiungo il sito, in caso contrario lo creo nuovo
		if(!cond){
			list=new ArrayList<Site>();
			list.add(site);
			sites=new UserSites(list);
		}
		else{
			Lock readLock = lockManager.getReadLock(path);
			readLock.lock();
			try(ObjectInputStream ois=new ObjectInputStream(new FileInputStream(path))){
				sites=(UserSites)ois.readObject();
				sites.sitesList.add(site);
			}
			catch(Exception e){
				e.printStackTrace();
				return false;
			}
			finally{
				readLock.unlock();
			}
		}

		Lock writeLock = lockManager.getWriteLock(path);
		writeLock.lock();
		try(ObjectOutputStream oos=new ObjectOutputStream(new FileOutputStream(path));){
			oos.writeObject(sites);
		}
		catch(Exception e){
			e.printStackTrace();
			return false;
		}
		finally{
			writeLock.unlock();
		}
		return true;
	}


	/*restituisce l'oggetto User dato il suo username*/
	private User getUser(String username){
		Lock readLock = lockManager.getReadLock(fileUsers);
		readLock.lock();
		try(ObjectInputStream ois=new ObjectInputStream(new FileInputStream(fileUsers));){
			Credentials c=(Credentials)ois.readObject();
			for (int i=0;i<c.userList.size();i++){
				if(c.userList.get(i).username.equals(username)){
					return c.userList.get(i);
				}
			}
			return null;
		}
		catch(Exception e){e.printStackTrace();}
		finally{
			readLock.unlock();
		}
		return null;
	}


	/*Resetta la lista dei timestamp dell'utente*/
	public void resetTimestampUser(String username){
		User user = getUser(username);
		if(user!=null){	//controllo prima se l'utente esiste
		for (int i=0;i<5 ;i++ ) {
			user.timestampList[i]=null;
		}
		List<User> newlist = getUserList();
		ListIterator<User> iterator = newlist.listIterator();
		while (iterator.hasNext()) {
			if (iterator.next().username.compareTo(user.username)==0) {
				iterator.remove();
				break;
			}
		}
		newlist.add(user);
		setUsersList(newlist);
	}
}


/*Controlla i timestamp dell'utente. Se l'utente ha effettuato più di 5 tentativi di inserimento password in un arco 5 minuti restituisce false*/
public boolean checkTimestampUser(String username){
	User user=getUser(username);
	if(user==null)//controllo prima se l'utente esiste
	return false;

	int maxAttempts = user.timestampList.length;
	ZoneId zoneId = ZoneId.of("Europe/Paris");
	LocalDateTime timestamp=LocalDateTime.now(zoneId);
	int cont=0;
	int recentlyTimestamp=0;
	int diff = 0;
	boolean findRecentlyTimestamp = false;

	while((cont<maxAttempts)&&(user.timestampList[cont]!=null))	{//conto i timestamp e setto il primo timestamp più recente
		diff=timestamp.minusMinutes(5).compareTo(user.timestampList[cont]);
		if((diff<0)&&(findRecentlyTimestamp==false)) {
			findRecentlyTimestamp = true;
			recentlyTimestamp = cont;	//primo timestamp più recente
		}
		cont++;
	}

	if(findRecentlyTimestamp==false)	//se non esiste quello più recente lo setto a cont (cioè quello che sto per inserire)
		recentlyTimestamp=cont;

	for (int i=0; i<maxAttempts; i++ ) {	//aggiorno lista timestamp, tenendo solo i timestamp recenti
		if((recentlyTimestamp + i)<maxAttempts)
			user.timestampList[i]=user.timestampList[recentlyTimestamp+i];
		else
			user.timestampList[i]=null;
	}

	int timestampIndex = cont - recentlyTimestamp;	//posizione in cui inserire il nuovo timestamp dopo aver aggiornato la lista dei timestamp

	if(timestampIndex==maxAttempts){	//l'utente ha finito le richieste disponibili perchè la posizione in cui inserire è uguale alla dimensione della lista
	System.out.println("Password entry: attempt number "+ (timestampIndex+1));
	return false;
}
else{
	System.out.println("Password entry: attempt number "+ (timestampIndex+1));
	user.timestampList[timestampIndex]=timestamp;
}
List<User> newlist=getUserList();
ListIterator<User> it = newlist.listIterator();
while (it.hasNext()) {
	if (it.next().username.compareTo(user.username)==0) {
		it.remove();
		break;
	}
}
newlist.add(user);
setUsersList(newlist);
return true;
}


/*Restituisce la lista degli utenti registrati*/
private List<User> getUserList(){
	Lock readLock = lockManager.getReadLock(fileUsers);
	readLock.lock();
	try(ObjectInputStream ois=new ObjectInputStream(new FileInputStream(fileUsers));){
		Credentials c=(Credentials)ois.readObject();
		return c.userList;
	}
	catch(Exception e){e.printStackTrace();}
	finally{
		readLock.unlock();
	}
	return null;
}
}
