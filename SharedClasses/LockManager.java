import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.Lock;

public class LockManager{

  private ArrayList<FileLock> fileLockList = new ArrayList<>();

  public Lock getWriteLock(String file){
    for(FileLock fileLock : fileLockList){
      if(fileLock.getFileName().compareTo(file)==0){
        return fileLock.getWriteLock();
      }
    }
    FileLock fileLock = new FileLock(file);
    fileLockList.add(fileLock);
    return fileLock.getWriteLock();
  }

  public Lock getReadLock(String file){
    for(FileLock fileLock : fileLockList){
      if(fileLock.getFileName().compareTo(file)==0){
        return fileLock.getReadLock();
      }
    }
    FileLock fileLock = new FileLock(file);
    fileLockList.add(fileLock);
    return fileLock.getReadLock();
  }

}
