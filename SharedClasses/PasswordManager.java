
import java.util.*;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import java.nio.charset.Charset;
import java.security.AlgorithmParameters;
import java.security.InvalidAlgorithmParameterException;
import java.security.spec.InvalidParameterSpecException;
import java.util.Base64;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class PasswordManager {

    private final int keyLength;
    private final int keyIterations;
    private Cipher cipher;
    private String masterPassword;
    private String salt;
    private java.util.Base64.Decoder decoder;
    private java.util.Base64.Encoder encoder;


    public PasswordManager(int keyLength, int keyIterations) {
      this.decoder = java.util.Base64.getDecoder();
      this.encoder = java.util.Base64.getEncoder();
      this.keyLength = keyLength;
      this.keyIterations = keyIterations;
    }

    public PasswordManager(int keyLength, int keyIterations, String algorithm) {
      this(keyLength,keyIterations);
      try {
            this.cipher = Cipher.getInstance(algorithm); //uso aes 256 bit con cbc
          }
          catch (NoSuchAlgorithmException | NoSuchPaddingException ex) {
            Logger.getLogger(PasswordManager.class.getName()).log(Level.SEVERE, null, ex);
          }
    }



    public void setMasterPassword(String masterPassword){
      this.masterPassword = masterPassword;
    }


    public void setSalt(String salt){
      this.salt = salt; //Uso un salt casuale
    }


    public ArrayList<String> encryptText(String plainText){
      try {
        ArrayList<String> encryptReturn= new ArrayList<>();
        SecretKey passwordKey = getPasswordKey();
        this.cipher.init(Cipher.ENCRYPT_MODE, passwordKey); //cipher in modalità di cifratura
        // System.out.println("The used algorithm to encrypt the saved passwords is --> " + cipher.getAlgorithm() + "");
        AlgorithmParameters params = this.cipher.getParameters();
        byte[] ivBytes = params.getParameterSpec(IvParameterSpec.class).getIV();  //Leggo IV per salvarlo nel server insieme alla password e usarlo nella decifratura
        byte[] cipherText = cipher.doFinal(plainText.getBytes()); //vuole un array di byte come parametro

        encryptReturn.add(encoder.encodeToString(cipherText));
        encryptReturn.add(encoder.encodeToString(ivBytes));
        return encryptReturn;
      }
      catch (InvalidKeyException | IllegalBlockSizeException | BadPaddingException | InvalidParameterSpecException ex) {
        Logger.getLogger(PasswordManager.class.getName()).log(Level.SEVERE, null, ex);
      }
      return null;
    }


    public String decryptText(String cipherText, String iv){
        try {
            SecretKey passwordKey = getPasswordKey();
            this.cipher.init(Cipher.DECRYPT_MODE, passwordKey, new IvParameterSpec(decoder.decode(iv)));
            byte[] plainText = cipher.doFinal(decoder.decode(cipherText));
            return new String(plainText);
        }
        catch (InvalidKeyException | IllegalBlockSizeException | BadPaddingException | InvalidAlgorithmParameterException ex) {
            Logger.getLogger(PasswordManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }


    /*Funzione che genera una chiave a partire da una password usando PBKDF2WithHmacSHA256*/
    private SecretKey getSecretByPassword(int iterations, String password, String salt, int keyLenght){
        try {
            int saltLength = salt.length(); // Bytes di salting dell'username
            PBEKeySpec keySpec = new PBEKeySpec(password.toCharArray(), this.decoder.decode(salt), iterations, keyLength); //Specifiche della chiave password-based encryption (PBE). Vuole un array di char perchè la stringa è costante
            SecretKeyFactory secretKeyFactory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256"); //Restituisce un oggetto SecretKeyFactory che converte le chiavi dell'algoritmo specificato
            SecretKey secretByPassword = secretKeyFactory.generateSecret(keySpec); //Chiave di cifratura generata dalla password con l'algoritmo specificato sopra
            // System.out.println("Algorithm for this key --> " + passwordKey.getAlgorithm()); //Darà PBKDF2WithHmacSHA256
            return secretByPassword;
        } catch (NoSuchAlgorithmException | InvalidKeySpecException ex) {
            Logger.getLogger(PasswordManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }


    /*Uso PBKDF2WithHmacSHA256 con N round per generare la chiave simmetrica a partire dalla password.
     La chiave viene usata con l'algoritmo AES per cifrare le password dei siti da salvare nel server.*/
    private SecretKey getPasswordKey(){
        System.out.println("\n-------key generation-------\n");
        SecretKey secretByPassword = getSecretByPassword(this.keyIterations, this.masterPassword, this.salt, this.keyLength);
        SecretKeySpec passwordKey = new SecretKeySpec(secretByPassword.getEncoded(), "AES");  //Può essere utilizzato per costruire un SecretKey da un array di byte, senza dover passare attraverso un SecretKeyFactory. Così da assocciare alla chiave l'algortimo AES, cambiandone le specifiche
        // System.out.println("Algorithm for this key --> " + secret.getAlgorithm()); //Darà AES
        System.out.println("The key generated by the master password is --> " + Base64.getEncoder().encodeToString(passwordKey.getEncoded()));
        return passwordKey;
    }


    /*Uso sempre PBKDF2WithHmacSHA256  per generare la funzione hash da inviare al server per autenticarsi
     ma con il doppio dei round di quelli usati nella generazione della chiave*/
    public String getPasswordHash(){
      SecretKey secretByPassword = getSecretByPassword(this.keyIterations*2, this.masterPassword, this.salt, this.keyLength);
      String passwordHash = encoder.encodeToString(secretByPassword.getEncoded());
      return passwordHash;
    }


  }
