import java.io.*;
public class ServerResponseMessage implements Serializable{
	public String text;
	public String messageType;
	public ServerResponseMessage(String type,String text){
    this.messageType = type;
    this.text = text;
	}

  public boolean getRequestResult()
  {
    return (this.messageType.compareTo("OK")==0);
  }
}
