import java.io.*;
import java.lang.String.*;
import java.time.*;
public class User implements Serializable{
	public String username;
	public String hashPassword;
	public String salt;	//Il salt viene settato alla registrazione e inviato all'utente ad ogni autenticazione
	public LocalDateTime timestampList[]=new LocalDateTime[5];
	public User(String user,String pass,String s){
		username=user;
		hashPassword=pass;
		salt=s;
		for (int i=0;i<5 ;i++ ) {
			timestampList[i]=null;
		}
	}
}
