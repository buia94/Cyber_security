import java.util.*;
public class GenerateCredentialsFile{

	private static final int byteNumber = 8;
	private static final int keyLength = 256;
	private static final int keyIterations = 65536;

	static final String[] userNames = {"mariorossi@gmail.com","mariobianchi@gmail.com"};
	static final String[] userPasswords = {"mariorossi94","mariobianchi94"};
	static ArrayList<String> userHashPassword = new ArrayList<String>();
	static ArrayList<String> userDoubleHashPassword = new ArrayList<String>();
	static ArrayList<String> userSalt = new ArrayList<String>();


	public static void main(String [] args){
		PasswordManager passwordManager = new PasswordManager(keyLength, keyIterations);
		SaltGenerator saltGenerator = new SaltGenerator(byteNumber);
		FileManager fileManager = new FileManager("./../Server/Utenti/","./../Server/credentials.bin");
		List<User> userList=new ArrayList<User>();

		for(int i=0; i<userNames.length;i++){
			String salt = saltGenerator.generateSalt();
			userSalt.add(salt);
			passwordManager.setSalt(salt);
			passwordManager.setMasterPassword(userPasswords[i]);
			System.out.println("\n-------hash generation-------\n");
			String passwordHash = passwordManager.getPasswordHash();
			userHashPassword.add(passwordHash);
		}

		for(int i=0; i<userNames.length;i++){
			passwordManager.setSalt(userSalt.get(i));
			passwordManager.setMasterPassword(userHashPassword.get(i));
			System.out.println("\n-------double hash generation-------\n");
			String doubleHashPassword = passwordManager.getPasswordHash();
			userDoubleHashPassword.add(doubleHashPassword);
			//System.out.println("HASH con 2N + 2N ripetizioni separate--> " + doubleHashPassword);
			userList.add(new User(userNames[i],doubleHashPassword,userSalt.get(i)));
		}

		fileManager.setUsersList(userList);

		for(int i=0; i<userNames.length;i++){
			if(fileManager.checkUser(userNames[i],userDoubleHashPassword.get(i))){
				System.out.println("Utente "+userNames[i]+" presente con double hash --> "+userDoubleHashPassword.get(i));
			}
		}

	}
}
