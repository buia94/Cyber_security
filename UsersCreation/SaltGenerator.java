
import java.security.SecureRandom;
import java.util.Arrays;

public class SaltGenerator {

  private final int byteNumber;

  public SaltGenerator(int byteNumber){
    this.byteNumber = byteNumber;
  }

  public String generateSalt() {
    SecureRandom random = new SecureRandom();
    byte bytes[] = new byte[this.byteNumber];
    System.out.println("\n-------salt generation-------\n");
    random.nextBytes(bytes);  //genera numero random e lo inserisce in bytes. Il seed se non lo settiamo se lo genera da solo.
    // System.out.println("1) Il salt in array di byte generato è --> " + Arrays.toString(bytes));
    java.util.Base64.Encoder encoder = java.util.Base64.getEncoder();
    String salt = encoder.encodeToString(bytes);
    // System.out.println("2) Il salt in stringa generato è --> " + salt);
    // java.util.Base64.Decoder decoder = java.util.Base64.getDecoder();
    // System.out.println("3) Il salt in array di byte è --> " + Arrays.toString(decoder.decode(salt)));
    return salt;
    }

}
